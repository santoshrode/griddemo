
var services = angular.module('exampleApp.services', ['ngResource']);

services.factory('loginService', function($resource) {

	return $resource('rest/piechart/:id', {id : '@paymentTypeID'});
});

services.factory('EmployService', function($resource) {

	return $resource('rest/employee/:id', {id : '@paymentTypeID'});
});

services.factory('barChartService', function($resource) {

	return $resource('rest/barchart/:id', {id : '@paymentTypeID'});
});

