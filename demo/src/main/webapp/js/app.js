var app =  angular.module('exampleApp', ['ui.grid.selection', 'ui.grid.exporter','nvd3','ui.bootstrap','ui.grid', 'ui.grid.pagination','n3-pie-chart','ngRoute','exampleApp.services'])
			.config(
				[ '$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider,$httpProvider) {

					$routeProvider.when('/login', {
						templateUrl: 'pages/login.html',
						controller: LoginController
					});
					
					$routeProvider.otherwise({
						templateUrl: 'pages/index.html',
						controller: IndexController
					});
					
					$routeProvider.when('/grid',{
						templateUrl: 'pages/grid.html',
						controller: gridController
					});
					
					
					
					$locationProvider.hashPrefix('!');
					

				} ]
				
			).run(function($rootScope, $location) {
				/* Reset error when a new view is loaded */
				$rootScope.$on('$viewContentLoaded', function() {
					delete $rootScope.error;
				});
				
				$rootScope.hasRole = function(role) {
					if ($rootScope.user === undefined) {
						return false;
					}
					if ($rootScope.user.roles[role] === undefined) {
						return false;
					}
					return $rootScope.user.roles[role];
				};

				/* Try getting valid user from cookie or go to login page */
				var originalPath = $location.path();
				$location.path("/login");
				$rootScope.initialized = true;
			});

