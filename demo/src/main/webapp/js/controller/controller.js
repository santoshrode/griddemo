function IndexController($scope){
	
	
}

function LoginController($scope,  $location, loginService ,$http,barChartService) {
	$scope.data =loginService.query();
	$scope.data1 =barChartService.query();

/* Pie Chart */
	               $scope.options = {
	                         chart: {
	                             type: 'pieChart',
	                             height: 500,
	                             x: function(d){return d.label;},
	                             y: function(d){return d.value;},
	                             showLabels: true,
	                             transitionDuration: 500,
	                             labelThreshold: 0.01,
	                             legend: {
	                                 margin: {
	                                     top: 5,
	                                     right: 35,
	                                     bottom: 5,
	                                     left: 0
	                                 }
	                             }
	                         }
	                     };

	         
	             /*Bar Chart */
	               $scope.options1 = {
	                           chart: {
	                               type: 'discreteBarChart',
	                               height: 450,
	                               margin : {
	                                   top: 20,
	                                   right: 20,
	                                   bottom: 60,
	                                   left: 55
	                               },
	                               x: function(d){return d.name;},
	                               y: function(d){return d.value;},
	                               showValues: true,
	                               valueFormat: function(d){
	                                   return d3.format(',.4f')(d);
	                               },
	                               transitionDuration: 500,
	                               xAxis: {
	                                   axisLabel: 'X Axis'
	                               },
	                               yAxis: {
	                                   axisLabel: 'Y Axis',
	                                   axisLabelDistance: 30
	                               }
	                           }
	                       };

	                       $scope.data1 = [
	                           {
	                               key: "Cumulative Return",
	                               values: $scope.data1 }]
	         
};


function gridController($scope,  $location,$http,EmployService) {
	 /*Grid Table*/
	
	   $scope.gridOptions1 = {
           	enableFiltering:true,
           	 enableGridMenu: true,
                enableSelectAll: true,
                paginationPageSizes: [15, 30, 60],
                paginationPageSize: 15,
                columnDefs: [
                  { name: 'employeeName' },
                  { name: 'age' },
                  { name: 'birthDate' },
                  { name: 'salary' },
                  { name: 'phoneNumber'},
                  { name: 'location' }
                ],   
                exporterCsvFilename: 'myFile.csv',
                exporterPdfDefaultStyle: {fontSize: 9},
                exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                exporterPdfFooter: function ( currentPage, pageCount ) {
                  return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                },
                exporterPdfCustomFormatter: function ( docDefinition ) {
                  docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
                  docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
                  return docDefinition;
                },
                exporterPdfOrientation: 'portrait',
                exporterPdfPageSize: 'LETTER',
                exporterPdfMaxGridWidth: 500,
                exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                onRegisterApi: function(gridApi){ 
                  $scope.gridApi = gridApi;
                }
              },
                      
                      
            $scope.gridOptions1.data =EmployService.query();
	
}

