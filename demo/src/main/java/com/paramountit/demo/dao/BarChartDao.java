package com.paramountit.demo.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.paramountit.demo.entity.BarChart;

@Repository
public class BarChartDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<BarChart> getAll(){
		return sessionFactory.getCurrentSession().createQuery("From BarChart").list();
	}
}
