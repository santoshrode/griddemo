package com.paramountit.demo.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.paramountit.demo.entity.PieChart;

@Repository
public class PieChartDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<PieChart> getAll() {
		return sessionFactory.getCurrentSession().createQuery("From PieChart")
				.list();
	}
}
