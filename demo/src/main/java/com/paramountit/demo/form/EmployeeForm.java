package com.paramountit.demo.form;

import java.sql.Date;

public class EmployeeForm {

	private Long id;
	private String employeeName;
	private Date birthDate;
	private Double salary;
	private Long phoneNumber;
	private String location;

	public Long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "EmployeeForm [id=" + id + ", employeeName=" + employeeName
				+ ", birthDate=" + birthDate + ", salary=" + salary
				+ ", phoneNumber=" + phoneNumber + ", location=" + location
				+ "]";
	}
	
}
