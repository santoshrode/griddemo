package com.paramountit.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paramountit.demo.dao.BarChartDao;
import com.paramountit.demo.entity.BarChart;

@Service
public class BarChartService {

	@Autowired
	private BarChartDao barChartDao;
	
	@Transactional
	public List<BarChart> getAll(){
		return barChartDao.getAll();
	}
}
