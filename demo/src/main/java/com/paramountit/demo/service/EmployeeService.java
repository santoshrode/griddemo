package com.paramountit.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.paramountit.demo.dao.EmployeeDao;
import com.paramountit.demo.entity.Employee;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
    
	@Transactional(readOnly=true)
	public List<Employee> getAll() {
		return employeeDao.getAll();
	}
}
