package com.paramountit.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.paramountit.demo.dao.PieChartDao;
import com.paramountit.demo.entity.PieChart;

@Service
public class PieChartService {

	@Autowired
	private PieChartDao pieChartDao;
    
	@Transactional(readOnly=true)
	public List<PieChart> getAll() {
		return pieChartDao.getAll();
	}
}
