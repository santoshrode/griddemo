package com.paramountit.demo.controller;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.paramountit.demo.entity.BarChart;
import com.paramountit.demo.service.BarChartService;

@Controller
@Path("/barchart")
public class BarChartController {

	private Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BarChartService barChartService;
	
	@Autowired
	private ObjectMapper mapper;
	
	/**
	 * This method is used to get bar-chart data
	 * @return This returns bar-chart data in json format
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAll() throws JsonGenerationException, JsonMappingException, IOException{
		logger.info("get All from bar-chart");
		ObjectWriter writter;
		writter =mapper.writer().withDefaultPrettyPrinter();
		List<BarChart> barChart=barChartService.getAll();
		return writter.writeValueAsString(barChart);
	}
}
