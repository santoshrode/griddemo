CREATE DATABASE  IF NOT EXISTS `demo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `demo`;
-- MySQL dump 10.14  Distrib 5.5.39-MariaDB, for Linux (i686)
--
-- Host: 127.0.0.1    Database: demo
-- ------------------------------------------------------
-- Server version	5.5.39-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barchart`
--

DROP TABLE IF EXISTS `barchart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barchart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barchart`
--

LOCK TABLES `barchart` WRITE;
/*!40000 ALTER TABLE `barchart` DISABLE KEYS */;
INSERT INTO `barchart` VALUES (1,'A',-29.765957771107),(2,'A',-29.765957771107),(3,'B',0),(4,'C',32.807804682612),(5,'D',196.45946739256),(6,'E',0.19434030906893),(7,'F',-98.079782601442),(8,'G',-13.925743130903),(9,'H',5.1387322875705);
/*!40000 ALTER TABLE `barchart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeName` varchar(45) DEFAULT NULL,
  `age` varchar(45) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `phoneNumber` bigint(20) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'john','76','1973-12-31',60000,9980777777,'London'),(2,'sheferd','67','1960-12-11',70000,9545454554,'Paris'),(3,'Moroni','50','1970-08-10',70000,7666664566,'Pune'),(4,'Tiancum','43','1985-02-12',60000,8888868886,'Banglore'),(5,'Jacob','27','1983-08-23',50000,7588888588,'Ranchi'),(6,'Nephi','29','1982-05-31',40000,5454675775,'Belgaum'),(7,'Enos','34','1986-08-03',30000,7829355657,'Karad'),(8,'Moroni','50','1970-10-28',60000,8660000066,'kolumbo'),(9,'Tiancum','43','1985-02-12',70000,7645454545,'Delhi'),(10,'jolly','43','1987-02-14',50000,5553535767,'Ahmedabad'),(11,'Adam','43','1984-06-12',80000,9877546544,'Ranchi'),(12,'Johnson','49','1981-03-12',60000,9878676765,'Toranto'),(13,'Johnson','39','1991-03-06',40000,7676868686,'Chennai'),(14,'George','29','1993-03-26',30000,3454553466,'Panaji'),(15,'George','32','1971-04-16',50000,7856456546,'Chikkodi'),(16,'john','76','1973-12-31',60000,9980777777,'London'),(17,'sheferd','67','1960-12-11',70000,9545454554,'Paris'),(18,'Moroni','50','1970-08-10',70000,7666664566,'Pune'),(19,'Moroni','50','1970-08-10',70000,7666664566,'Pune'),(20,'Nephi','29','1982-05-31',40000,5454675775,'Belgaum'),(21,'Enos','34','1986-08-03',30000,7829355657,'Karad'),(22,'Moroni','50','1970-10-28',60000,8660000066,'kolumbo'),(23,'Tiancum','43','1985-02-12',70000,7645454545,'Delhi'),(24,'jolly','43','1987-02-14',50000,5553535767,'Ahmedabad'),(25,'Adam','43','1984-06-12',80000,9877546544,'Ranchi'),(26,'Johnson','49','1981-03-12',60000,9878676765,'Toranto'),(27,'Johnson','39','1991-03-06',40000,7676868686,'Chennai'),(28,'George','29','1993-03-26',30000,3454553466,'Panaji'),(29,'George','32','1971-04-16',50000,7856456546,'Chikkodi'),(30,'Moroni','50','1970-08-10',70000,7666664566,'Pune'),(31,'Nephi','29','1982-05-31',40000,5454675775,'Belgaum'),(32,'Enos','34','1986-08-03',30000,7829355657,'Karad'),(33,'Moroni','50','1970-10-28',60000,8660000066,'kolumbo'),(34,'Tiancum','43','1985-02-12',70000,7645454545,'Delhi'),(35,'jolly','43','1987-02-14',50000,5553535767,'Ahmedabad'),(36,'Adam','43','1984-06-12',80000,9877546544,'Ranchi'),(37,'Johnson','49','1981-03-12',60000,9878676765,'Toranto'),(38,'Johnson','39','1991-03-06',40000,7676868686,'Chennai'),(39,'George','29','1993-03-26',30000,3454553466,'Panaji'),(40,'George','32','1971-04-16',50000,7856456546,'Chikkodi'),(41,'Moroni','50','1970-08-10',70000,7666664566,'Pune'),(42,'Nephi','29','1982-05-31',40000,5454675775,'Belgaum'),(43,'Enos','34','1986-08-03',30000,7829355657,'Karad'),(44,'Moroni','50','1970-10-28',60000,8660000066,'kolumbo'),(45,'Tiancum','43','1985-02-12',70000,7645454545,'Delhi'),(46,'jolly','43','1987-02-14',50000,5553535767,'Ahmedabad'),(47,'Adam','43','1984-06-12',80000,9877546544,'Ranchi'),(48,'Johnson','49','1981-03-12',60000,9878676765,'Toranto'),(49,'Johnson','39','1991-03-06',40000,7676868686,'Chennai'),(50,'George','29','1993-03-26',30000,3454553466,'Panaji'),(51,'George','32','1971-04-16',50000,7856456546,'Chikkodi');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piechart`
--

DROP TABLE IF EXISTS `piechart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piechart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  `value` double DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piechart`
--

LOCK TABLES `piechart` WRITE;
/*!40000 ALTER TABLE `piechart` DISABLE KEYS */;
INSERT INTO `piechart` VALUES (1,'Work',11,'#00ff00'),(2,'Eat',2,'rgb(0, 0, 255)'),(3,'Commute',2,'red'),(4,'Watch TV',2,'yellow'),(5,'Sleep',7,'pink');
/*!40000 ALTER TABLE `piechart` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-14 15:49:29